import matplotlib.pyplot as plt
import numpy as np
from scipy.io import loadmat
from scipy.signal import butter, sosfiltfilt, sosfreqz


def butter_lowpass(cut, fs, order):
    return butter(order, cut, fs=fs, output="sos")


def butter_lowpass_filter(data, fs, cut, order):
    return sosfiltfilt(butter_lowpass(cut, fs, order=order), data)


# data inladen
data = loadmat("sprinttest.mat")["speed"].squeeze()

fs = 400.0
T = 1 / fs
N = len(data)

tijd = np.linspace(0, N*T, N, endpoint=False)

# plot de ruw data
plt.figure()
plt.plot(tijd, data)
plt.plot(tijd, butter_lowpass_filter(data, fs=400., cut=10,  order=2))
plt.xlabel("tijd (s)")
plt.ylabel("snelheid (m/s)")
plt.title("sprint-test grasbaan")

# plot de filtereigenschappen
sos = butter_lowpass(10, fs, order=8)
w, h = sosfreqz(sos, worN=2000)

plt.figure()
plt.plot((fs * 0.5 / np.pi) * w, abs(h))
plt.plot([0, 0.5 * fs], [np.sqrt(0.5), np.sqrt(0.5)], '--')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Gain')
plt.title("Eight order 10Hz Butterworth response")
plt.xlim(0, 200)
