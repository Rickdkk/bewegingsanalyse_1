# This function rotates a spoke wheel image by degPerFrame degrees a number
# of times. A positive degPerFrame rotates the image clockwise, a negative
# degPerFrame rotates the image counterclockwise.

from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.animation import FuncAnimation

n_frames = 100
deg_per_frame = 45.0
img = mpimg.imread("spokeWheel.tif")

fig, ax = plt.subplots(nrows=1, ncols=1)
fig.set_size_inches(6, 6, forward=True)  # make figure square
fig.subplots_adjust(left=0, bottom=0, right=1, top=1,
                    wspace=None, hspace=None)  # remove white margins
ax.set_facecolor((0, 0, 0, 1))  # use black background color
ax.set_axis_off()  # hide axes


def update(i):
    ax.imshow(ndimage.rotate(img, i * -deg_per_frame, reshape=False))


anim = FuncAnimation(fig, update, interval=100)  # approx 10fps
plt.show()
