#%%
import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
from scipy.io import savemat

# Number of samplepoints
N = 10000
# sample spacing
T = 1.0 / 100.0
x = np.linspace(0.0, N*T, N)
y = np.sin(1.2 * 2.0*np.pi*x) + 0.5*np.sin(5.0 * 2.0*np.pi*x) + np.random.randint(-10, 10, N) * 0.05
# savemat("signaal", mdict={"signaal": y})

plt.plot(2, 1, 1)
plt.plot(x, y, "C1", linewidth=0.7)
plt.xlim(0, 10)
plt.xlabel("Tijd (s)")
plt.ylabel("afstand (m)")
plt.title("X-positie handmarker")

plt.show()
