#%%
import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from scipy.integrate import cumtrapz
from scipy.io import savemat, loadmat


# test diff and gradient
h = 0.01  # stap grootte
X = np.arange(0, 2*np.pi + h, h)  # domein
f = np.sin(X)  # functie

Yd = np.diff(f) / h  # eerste afgeleide - diff
Zd = np.diff(Yd) / h  # tweede afgeleide
Yg = np.gradient(f) / h  # eerste afgeleide - gradient
Zg = np.gradient(Yg) / h  # tweede afgeleide

# plot de twee methoden
plt.close("all")
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f)
plt.plot(X[:-1], Yd)  # dit zou een cosinus moeten zijn
plt.plot(X[:-2], Zd)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f)
plt.plot(X, Yg)
plt.plot(X, Zg)

# hetzelfde op een signaal met ruis
f_noise = f + np.random.normal(0, 0.00001, len(f))  # voeg wat normaal verdeelde ruis toe
Yd_noise = np.diff(f_noise) / h  # eerste afgeleide - diff
Zd_noise = np.diff(Yd_noise) / h  # tweede afgeleide
Yg_noise = np.gradient(f_noise) / h  # eerste afgeleide - gradient
Zg_noise = np.gradient(Yg_noise) / h  # tweede afgeleide

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f_noise)
plt.plot(X[:-1], Yd_noise)  # dit zou een cosinus moeten zijn
plt.plot(X[:-2], Zd_noise)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f_noise)
plt.plot(X, Yg_noise)
plt.plot(X, Zg_noise)

# reconstrueer het originele signaal met cumtrapz
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f_noise)
plt.subplot(2, 1, 2)
plt.plot(X, cumtrapz(Yg_noise, initial=0) * 0.01)
plt.show()

print(np.allclose(f_noise, cumtrapz(Yg_noise, initial=0) * 0.01, atol=0.0001))
# all( abs(a-b) <= atol+rtol*abs(b), 'all') or all(abs(a(:)-b(:)) <= atol+rtol*abs(b(:))) for matlab equivalent


#%% generated signal
def damped_sine(a, decay, phase, omega, t):
    sine = a * np.e**(-decay*t) * np.cos(omega*t + phase)
    return sine


time = np.arange(0, 7.51, 0.01)
my_sine = damped_sine(1, 0.25, -0.5*np.pi, 2*2*np.pi, time)
my_sine += np.random.normal(0, 0.01, len(my_sine))
# savemat("H7_signaal", {"signaal": my_sine})


# find peaks in signal
lcp, _ = find_peaks(my_sine, prominence=0.1)
lcm, _ = find_peaks(-my_sine, prominence=0.1)
peaks = np.concatenate((lcp, lcm))


# differentiate signal
diff1 = np.diff(my_sine, append=0)/0.01
diff2 = np.diff(np.diff(my_sine, append=0)/0.01, append=0)/0.01

grad1 = np.gradient(my_sine)/0.01
grad2 = np.gradient(np.gradient(my_sine)/0.01)/0.01


# plot results
plt.style.use("default")
fig = plt.figure(figsize=(6, 10))
plt.subplot(3, 1, 1)
plt.title("Marker tijdens trifilar pendulum test")
plt.plot(time, my_sine)
plt.plot(time, cumtrapz(np.gradient(my_sine), initial=0))
plt.plot(time[peaks], my_sine[peaks], 'ro', mfc='none')
plt.xlabel("tijd (s)")
plt.ylabel("afstand (m)")
plt.autoscale(axis="x", tight=True)

plt.subplot(3, 1, 2)
plt.plot(time, diff1, label="diff")
plt.plot(time, grad1, label="gradient")
plt.plot(time[peaks], np.zeros(len(peaks)), 'ro', mfc='none')
plt.xlabel("tijd (s)")
plt.ylabel("snelheid (m/s)")
plt.autoscale(axis="x", tight=True)
plt.legend(loc="upper right")

plt.subplot(3, 1, 3)
plt.plot(time, diff2, label="diff")
plt.plot(time, grad2, label="gradient")
plt.xlabel("tijd (s)")
plt.ylabel("versnelling (m/s/s)")
plt.autoscale(axis="x", tight=True)
plt.legend(loc="upper right")
plt.tight_layout()
fig.align_labels()
plt.show()
