import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import cumtrapz


#%% test diff and gradient
h = 0.01  # stap grootte
X = np.arange(0, 2*np.pi + h, h)  # domein
f = np.$$$$(X)  # functie

Yd = np.$$$$(f) / $$$$  # eerste afgeleide - diff
Zd = np.$$$$(Yd) / $$$$  # tweede afgeleide
Yg = np.$$$$(f) / $$$$  # eerste afgeleide - gradient
Zg = np.$$$$(Yg) / $$$$  # tweede afgeleide

# plot de twee methoden
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f)
plt.plot(X[$$$$], Yd)  # dit zou een cosinus moeten zijn
plt.plot(X[$$$$], Zd)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f)
plt.plot(X, Yg)
plt.plot(X, Zg)

#%% hetzelfde op een signaal met ruis
f_noise = f + np.random.normal(0, 0.00001, len($$$$))  # voeg wat normaal verdeelde ruis toe
Yd_noise = np.$$$$(f_noise) / $$$$  # eerste afgeleide - diff
Zd_noise = np.$$$$(Yd_noise) / $$$$  # tweede afgeleide
Yg_noise = np.$$$$(f_noise) / $$$$  # eerste afgeleide - gradient
Zg_noise = np.$$$$(Yg_noise) / $$$$  # tweede afgeleide

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, $$$$)
plt.plot(X[$$$$], Yd_noise)  # dit zou een cosinus moeten zijn
plt.plot(X[$$$$], Zd_noise)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f_noise)
plt.plot(X, Yg_noise)
plt.plot(X, Zg_noise)

#%% reconstrueer het originele signaal met cumtrapz
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f_noise)

plt.subplot(2, 1, 2)
plt.plot(X, $$$$(Yg_noise, initial=0) * $$$$)  # plot hier het geintegreerde signaal
plt.show()

print(np.allclose(f_noise, $$$$(Yg_noise, initial=0) * $$$$, atol=0.0001))
