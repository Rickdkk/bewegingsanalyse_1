import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import cumtrapz


#%% test diff and gradient
h = 0.01  # stap grootte
X = np.arange(0, 2*np.pi + h, h)  # domein
f = np.sin(X)  # functie

Yd = np.diff(f) / h  # eerste afgeleide - diff
Zd = np.diff(Yd) / h  # tweede afgeleide
Yg = np.gradient(f) / h  # eerste afgeleide - gradient
Zg = np.gradient(Yg) / h  # tweede afgeleide

# plot de twee methoden
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f)
plt.plot(X[:-1], Yd)  # dit zou een cosinus moeten zijn
plt.plot(X[:-2], Zd)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f)
plt.plot(X, Yg)
plt.plot(X, Zg)

#%% hetzelfde op een signaal met ruis
f_noise = f + np.random.normal(0, 0.00001, len(f))  # voeg wat normaal verdeelde ruis toe
Yd_noise = np.diff(f_noise) / h  # eerste afgeleide - diff
Zd_noise = np.diff(Yd_noise) / h  # tweede afgeleide
Yg_noise = np.gradient(f_noise) / h  # eerste afgeleide - gradient
Zg_noise = np.gradient(Yg_noise) / h  # tweede afgeleide

plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f_noise)
plt.plot(X[:-1], Yd_noise)  # dit zou een cosinus moeten zijn
plt.plot(X[:-2], Zd_noise)  # dit zou -sinus moeten zijn

plt.subplot(2, 1, 2)
plt.plot(X, f_noise)
plt.plot(X, Yg_noise)
plt.plot(X, Zg_noise)

#%% reconstrueer het originele signaal met cumtrapz
plt.figure()
plt.subplot(2, 1, 1)
plt.plot(X, f_noise)

plt.subplot(2, 1, 2)
plt.plot(X, cumtrapz(Yg_noise, initial=0) * 0.01)
plt.show()

print(np.allclose(f_noise, cumtrapz(Yg_noise, initial=0) * 0.01, atol=0.0001))
