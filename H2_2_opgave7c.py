# Dit script plot de Fourrierreeks van opgave 7c Hoofdstuk2_deel2
import numpy as np
import matplotlib.pyplot as plt

grond_freq = 2*np.pi  # de grondfrequentie van de Fourrierreeks

n_harms = 6
harm_tot = 0
harms = []  # list voor harmonischen

tijd = np.linspace(0, 3, 3*100 + 1)  # tijdsbasis voor berekening van sinus- en cosinusfuncties

for idx in range(n_harms):
    idx += 1  # start bij 1 ipv 0
    a = -(2/(idx*np.pi))*(np.sin(idx*3*np.pi/4)-np.sin(idx*7*np.pi/4))
    b = (2/(idx*np.pi))*(np.cos(idx*3*np.pi/4)-np.cos(idx*7*np.pi/4))
    harms.append(a*np.cos(idx*grond_freq*tijd) + b*np.sin(idx*grond_freq*tijd))
    harm_tot += harms[-1]

# plot de afzonderlijke Fourriertermen en de opgetelde termen
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=True)
[ax1.plot(tijd, harm, label="harm_" + str(idx)) for idx, harm in enumerate(harms)]  # plot afzonderlijke Fourriertermen
ax1.legend(loc=1)
ax1.set_xlabel("tijd (s)")
ax1.set_title("afzonderlijke harmonischen")

ax2.plot(tijd, harm_tot)
ax2.set_xlabel("tijd (s)")
ax2.set_title(str(n_harms) + " Fourriertermen opgeteld")
ax2.autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
