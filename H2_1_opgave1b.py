# Script H2_1_opgave_1b

import matplotlib.pyplot as plt
import numpy as np

# Het argument van de cosinusfunctie wordt berekend voor verschillende
# waarden van 'x' op het interval [0 tot 15] rad
x = np.linspace(0, 15, 15*10+1)

# plot cosinus functies met verschillende AMPLITUDE
fig, (ax1, ax2) = plt.subplots(2, 1)
phase = 0  # fasehoek
amplitude = 2  # amplitude
ax1.plot(x, amplitude*np.cos(x + phase))
ax1.plot([x[0], x[-1]], [0, 0], '--k')  # plot nullijn

ax1.autoscale(axis='x', tight=True)
ax1.set_xlabel("x (rad)")
ax1.set_ylabel("f(x) = 2cos(x + 0)")

# plot sinus functie met fase
phase = 0.5*np.pi
ax2.plot(x, amplitude*np.sin(x + phase))
ax2.plot([x[0], x[-1]], [0, 0], '--k')  # plot nullijn

ax2.autoscale(axis='x', tight=True)
ax2.set_xlabel("x (rad)")
ax2.set_ylabel("f(x) = 2sin(x + 0.5pi)")

plt.tight_layout()
plt.show()
