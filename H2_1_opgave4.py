# Script H2_1_opgave_3b

import matplotlib.pyplot as plt
import numpy as np

# Het argument van de cosinusfunctie wordt berekend voor verschillende
# waarden van 't' op het interval [0 tot 10] s
t = np.linspace(0, 10, 10*10+1)  # nu in seconden

# plot f(t)
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharey=True)
phase = 0  # fasehoek
amplitude_sin = 3  # amplitude sinusfunctie
amplitude_cos = 2  # amplitude cosinusfunctie
hoek_freq = 1  # hoekfrequentie

ax1.plot(t, amplitude_sin*np.sin(hoek_freq * t + phase), label="3sin(t)")
ax1.plot(t, amplitude_cos*np.cos(hoek_freq * t + phase), label="2cos(t)")
ax1.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax1.autoscale(axis='x', tight=True)
ax1.set_xlabel("t (s)")
ax1.set_ylabel("sin en cosfunctie")
ax1.legend()

# plot y(t) oftewel de sommatie van de sinus- en cosinusfunctie
ax2.plot(t, amplitude_sin*np.sin(hoek_freq * t + phase) + amplitude_cos*np.cos(hoek_freq * t + phase))
ax2.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax2.autoscale(axis='x', tight=True)
ax2.set_xlabel("t (s)")
ax2.set_ylabel("y(t)")

# plot x(t)
phase = np.arctan(amplitude_cos/amplitude_sin)
amplitude = amplitude_cos/np.sin(phase)
ax3.plot(t, amplitude*np.sin(hoek_freq * t + phase))
ax3.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax3.autoscale(axis='x', tight=True)
ax3.set_xlabel("t (s)")
ax3.set_ylabel("x(t)")

plt.tight_layout()
plt.show()
