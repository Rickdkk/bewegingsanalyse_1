# Script H2_1_opgave_1a

import matplotlib.pyplot as plt
import numpy as np

# Het argument van de cosinusfunctie wordt berekend voor verschillende
# waarden van 'x' op het interval [0 tot 15] rad
x = np.linspace(0, 15, 100)  # let op: radialen

# plot cosinus functies met verschillende AMPLITUDE
fig, (ax1, ax2) = plt.subplots(1, 2)
phase = 0  # fasehoek
amplitude = 1  # amplitude
ax1.plot(x, amplitude*np.cos(x + phase), label="A = 1")
ax1.autoscale(axis='x', tight=True)

# plot cosinus met andere amplitude
amplitude = 2  # verander amplitude
ax1.plot(x, amplitude*np.cos(x + phase), label="A = 2")

# plot nullijn
ax1.plot([x[0], x[-1]], [0, 0], '--k')

# voorzie de subplot van geschikte labels
ax1.set_xlabel("x (rad)")
ax1.set_ylabel("f(x) = Acos(x + 0)")
ax1.set_title("Effect verschillende amplitude")
ax1.legend(loc=1)

# plot cosinus functies met verschillende FASEHOEK
ax2.plot(x, amplitude*np.cos(x + phase), label="B = 0")
ax2.autoscale(axis='x', tight=True)

# plot de cosinus met de andere fasehoek
phase = 0.6
ax2.plot(x, amplitude*np.cos(x + phase), label="B = 0.6")
ax2.plot([x[0], x[-1]], [0, 0], '--k')

# voorzie de subplot van geschikte labels
ax2.set_xlabel("x (rad)")
ax2.set_ylabel("f(x) = Acos(x + B)")
ax2.set_title("Effect verschillende fase")
ax2.legend(loc=1)

plt.tight_layout()
plt.show()
