# Dit script plot de Fourrierreeks van opgave 4a Hoofdstuk2_deel2
import numpy as np
import matplotlib.pyplot as plt

grond_freq =   # de grondfrequentie van de Fourrierreeks

coefs, harms = [], []  # lists voor Fourriercoefficienten en harmonischen
a0 =   # definieer de Fourriercoefficienten zoals berekend in de opgave
coefs.append([, ])    # a1, b1
coefs.append([, ])    # a2, b2
coefs.append([, ])  # a3, b3
coefs.append([, ])    # a4, b4
coefs.append([, ])  # a5, b5
coefs.append([, ])    # a6, b6
coefs.append([, ])  # a7, b7

tijd = np.linspace(0, 3, 3*100 + 1)  # tijdsbasis voor berekening van sinus- en cosinusfuncties

for h, (a, b) in enumerate(coefs):  # bereken grond-, eerste t/m 6de harmonische
    h += 1  # start bij 1
    harms.append(a*np.cos(h*grond_freq*tijd) + b*np.sin(h*grond_freq*tijd))

fourrierreeks = a0 + np.sum(harm for harm in harms)  # construeer de Fourrierreeks

# plot de afzonderlijke Fourriertermen en de opgetelde termen
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=True)
[ax1.plot(tijd, harm) for harm in harms]  # plot de afzonderlijke Fourriertermen
ax1.legend(["grond", "harm1", "harm2", "harm3", "harm4", "harm5", "harm6"], loc=1)
ax1.set_xlabel("tijd (s)")
ax1.set_title("afzonderlijke harmonischen")

ax2.plot(tijd, fourrierreeks)
ax2.set_xlabel("tijd (s)")
ax2.set_title("alle Fourriertermen opgeteld")
ax2.autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
