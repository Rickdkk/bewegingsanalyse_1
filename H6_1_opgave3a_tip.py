# Dit script bevat H6_1 opgave 3a

import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

data = sio.loadmat('normaalLopen.mat')["normaalLopen"]  # load and unpack
# structured like: data[:, x1;y1;x2;y2;etc]
xs = data[:,]  # elk tweede even getal
ys = data[:,]  # elk tweede oneven getal

tijd = np.linspace(0, data.shape[0]/100, data.shape[0])

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 5), sharex=True)

ax1.set_xlabel("tijd (s)")
ax1.set_ylabel("x-coordinaat (m)")
ax1.legend(["1x", "2x", "3x", "4x", "5x"], loc=1)


ax2.set_xlabel("tijd (s)")
ax2.set_ylabel("y-coordinaat (m)")
ax2.legend(["1y", "2y", "3y", "4y", "5y"], loc=1)
ax2.autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
