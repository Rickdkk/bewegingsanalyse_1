# hoofdstuk 4_2 opgave 5d

import numpy as np
import matplotlib.pyplot as plt

# freq_norm = vul hier de genormaliseerde frequentiewaarden in

# H = vul hier de berekening van H in

[print(f"genormaliseerde hoekfreq = {hoekfreq} , (omega0)^2.H = {h}") for hoekfreq, h in zip(freq_norm, H)]

plt.figure(figsize=(10, 6))
plt.loglog(freq_norm, H)
plt.ylabel(r"$\omega_0^2.H$")
plt.xlabel("genormaliseerde frequentie")
plt.title("bodeplot versnellingsopnemer als functie van de genormaliseerde frequentie")
plt.gca().autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
