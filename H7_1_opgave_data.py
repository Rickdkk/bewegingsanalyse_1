#%%
import pandas as pd
import matplotlib
matplotlib.use("WX")
import matplotlib.pyplot as plt
import numpy as np

column_names = ['timestamp', 'tag_id', 'x_pos', 'y_pos', 'heading', 'direction', 'energy', 'speed', 'total_distance']
df = pd.read_csv("./data/2013-11-28_tromso_tottenham.csv", names=column_names)
df["timestamp"] = pd.to_datetime(df["timestamp"])

#%%
selected_player = df[df.tag_id == 4].copy().reset_index(drop=True)  # select copy of random player
selected_player["timestamp"] = pd.to_timedelta(selected_player["timestamp"])
selected_player["seconds"] = selected_player["timestamp"].dt.total_seconds()

selected_player["seconds"] -= selected_player["seconds"][0]  # set inital time to 0
selected_player["total_distance"] -= selected_player["total_distance"][0]  # set inital distance to 0
# selected_player["total_distance"] = selected_player["total_distance"].rolling(window=3,center=False).mean()

selected_player["tot_pos"] = (selected_player.x_pos ** 2 + selected_player.y_pos ** 2) ** 0.5
selected_player["tot_speed"] = np.diff(selected_player["tot_pos"], prepend=74.95957) / 0.05
selected_player["tot_speed1"] = np.gradient(selected_player["tot_pos"]) / 0.05
selected_player["tot_speed2"] = np.gradient(selected_player["tot_pos"], 2) / 0.05

#%%
plt.close("all")
plt.figure()

plt.plot(selected_player.seconds, selected_player.tot_speed.abs(), label="diff")
plt.plot(selected_player.seconds, selected_player.tot_speed1.abs(), label="gradient")
plt.plot(selected_player.seconds, selected_player.tot_speed2.abs(), label="gradient3")
plt.xlabel("time(s)")
plt.ylabel("speed (m/s)")
plt.legend()
plt.tight_layout()
