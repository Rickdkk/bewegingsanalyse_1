# H2_2_opgave8
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
from scipy.io import loadmat

# laad de data
y = loadmat("signaal.mat")["signaal"][0]
N = len(y)
T = 1/100
x = np.linspace(0.0, N*T, N, endpoint=False)

# plot de data
plt.subplot(2, 1, 1)
plt.plot(x, y)
plt.xlabel("Tijd (s)")
plt.ylabel("afstand (m)")

# Bereken f(y) in het frequentiedomein
xf = np.linspace(0.0, 1.0/(2.0*T), N//2 + 1)
yf = fft(y)
yf = 2.0/N * np.abs(yf[:N//2+1])

# plot de Fourier transformatie
plt.subplot(2, 1, 2)
plt.plot(xf, yf)
plt.xlabel("Frequentie (Hz)")
plt.ylabel("Amplitude")

plt.tight_layout()
plt.show()
