# Script H2_1_opgave_3b

import matplotlib.pyplot as plt
import numpy as np

# Het argument van de cosinusfunctie wordt berekend voor verschillende
# waarden van 't' op het interval [0 tot 10] s
t = np.linspace(0, 10, 100)  # nu in seconden

# plot f(t)
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex="all")
phase = 1  # fasehoek
amplitude = 3  # amplitude
hoek_freq = 2  # hoekfrequentie
ax1.plot(t, amplitude*np.sin(hoek_freq * t + phase))
ax1.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax1.set_xlabel("t (s)")
ax1.set_ylabel("f(t)")

# plot sinus- en cosinusfunctie afzonderlijk
phase = 0
amplitude_sin = 1.621  # amplitude sinusfunctie
amplitude_cos = 2.524  # amplitude cosinusfunctie
ax2.plot(t, amplitude_sin*np.sin(hoek_freq * t + phase), label="1.621sin(2t)")
ax2.plot(t, amplitude_cos*np.cos(hoek_freq * t + phase), label="2.524cos(2t)")
ax2.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax2.set_xlabel("t (s)")
ax2.set_ylabel("sin/cos")
ax2.legend(loc=1)

# plot g(t) oftewel de sommatie van de sinus- en cosinusfunctie
ax3.plot(t, amplitude_sin*np.sin(hoek_freq * t + phase) + amplitude_cos*np.cos(hoek_freq * t + phase))
ax3.plot([t[0], t[-1]], [0, 0], '--k')  # plot nullijn

ax3.autoscale(axis='x', tight=True)
ax3.set_xlabel("t (s)")
ax3.set_ylabel("g(t)")

plt.tight_layout()
plt.show()
