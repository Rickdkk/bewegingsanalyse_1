# hoofdstuk 4_2 opgave 5d

import numpy as np
import matplotlib.pyplot as plt

freq_norm = np.array([0.00001, 0.0001, 0.001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                      0.7, 0.8, 0.9, 0.99, 0.999, 1.001, 1.01, 1.1, 2, 3, 4, 5, 6,
                      7, 8, 9, 10, 40, 100])

H = 1/np.abs(1 - freq_norm ** 2)

[print(f"genormaliseerde hoekfreq = {hoekfreq} , (omega0)^2.H = {h}") for hoekfreq, h in zip(freq_norm, H)]

plt.figure(figsize=(10, 6))
plt.loglog(freq_norm, H)
plt.ylabel(r"$\omega_0^2.H$")
plt.xlabel("genormaliseerde frequentie")
plt.title("bodeplot versnellingsopnemer als functie van de genormaliseerde frequentie")
plt.gca().autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
