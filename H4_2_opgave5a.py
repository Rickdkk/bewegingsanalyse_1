# hoofdstuk 4_2 opgave 5a

import numpy as np
import matplotlib.pyplot as plt

eigenfreq = 15000

freq = np.array([0.00001, 0.0001, 0.001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                 0.7, 0.8, 0.9, 0.99, 0.999, 1.001, 1.01, 1.1, 2, 3, 4, 5, 6,
                 7, 8, 9, 10, 40, 100]) * eigenfreq

H = 1/np.abs(eigenfreq**2 - freq**2)

[print(f"hoekfreq = {hoekfreq} , H = {h}") for hoekfreq, h in zip(freq, H)]

plt.figure(figsize=(10, 6))
plt.loglog(freq, H)
plt.ylabel("H", rotation=0)
plt.xlabel(r"$\omega$ (rad/s)")
plt.title(r"bodeplot versnellingsopnemer $\omega_0\ =\ 15.10^3$ rad/s")
plt.gca().autoscale(axis='x', tight=True)

plt.tight_layout()
plt.show()
